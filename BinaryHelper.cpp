#include "BinaryHelper.h"

void setUint64(unsigned char* c, unsigned int offset, uint64_t in) {
    for (int i = 0; i <= 7; i++) {
            c[i + offset] = (in >> (i*8));
        }
}

void setUint64(unsigned char* c, uint64_t in) {
    setUint64(c, 0, in);
}

void setUint64(std::vector<unsigned char>* v, unsigned int offset, uint64_t in) {
    if (v->size() < 8 + offset)
        v->resize(8 + offset);
    setUint64(v->data(), offset, in);
}

void setUint64(std::vector<unsigned char>* v, uint64_t in) {
    setUint64(v, 0, in);
}

uint64_t getUint64(unsigned char* c, unsigned int offset) {
    return static_cast<uint64_t>(c[7 + offset]) << 56 | static_cast<uint64_t>(c[6 + offset]) << 48 | static_cast<uint64_t>(c[5 + offset]) << 40 | static_cast<uint64_t>(c[4 + offset]) << 32 | static_cast<uint64_t>(c[3 + offset]) << 24 | static_cast<uint64_t>(c[2 + offset]) << 16 | static_cast<uint64_t>(c[1 + offset]) << 8 | static_cast<uint64_t>(c[offset]);
}

uint64_t getUint64(unsigned char* c) {
    return getUint64(c, 0);
}

uint64_t getUint64(std::vector<unsigned char>* v, unsigned int offset) {
    if (v->size() >= 8 + offset) {
        return getUint64(v->data(), offset);
    }
    return 0;
}

uint64_t getUint64(std::vector<unsigned char>* v) {
    return getUint64(v, 0);
}

void setUint16(unsigned char* c, unsigned int offset, uint16_t in) {
    c[offset] = in >> 0;
    c[1 + offset] = in >> 8;
}

void setUint16(unsigned char* c, uint16_t in) {
    setUint16(c, 0, in);
}

void setUint16(std::vector<unsigned char>* v, unsigned int offset, uint16_t in) {
    if (v->size() < 2 + offset)
        v->resize(2 + offset);
    setUint16(v->data(), offset, in);
}

void setUint16(std::vector<unsigned char>* v, uint16_t in) {
    setUint16(v, 0, in);
}

uint16_t getUint16(unsigned char* c, unsigned int offset) {
    return static_cast<uint16_t> (c[offset] | c[1 + offset] << 8);
}

uint16_t getUint16(unsigned char* c) {
    return getUint16(c, 0);
}

uint16_t getUint16(std::vector<unsigned char>* v, unsigned int offset) {
    if (v->size() >= 2 + offset)
        return getUint16(v->data(), offset);
    return 0;
}

uint16_t getUint16(std::vector<unsigned char>* v) {
    return getUint16(v, 0);
}

void setUint(unsigned char* c, unsigned int offset, unsigned int in) {
    c[offset] = in >> 0;
    c[1 + offset] = in >> 8;
    c[2 + offset] = in >> 16;
    c[3 + offset] = in >> 24;
}

void setUint(std::vector<unsigned char>* v, unsigned int in) {
    setUint64(v, 0, in);
}

void setUint(std::vector<unsigned char>* v, unsigned int offset, unsigned int in) {
    if (v->size() < 4 + offset)
        v->resize(4 + offset);
    setUint(v->data(), offset, in);
}

void setUint(std::vector<unsigned char>* v) {
    setUint(v, 0);
}

unsigned int getUint(unsigned char* c, int offset) {
    return static_cast<unsigned int> (c[offset] | c[1 + offset] << 8 | c[2 + offset] << 16 | c[3 + offset] << 24);
}

unsigned int getUint(unsigned char* c) {
    return getUint(c, 0);
}

unsigned int getUint(std::vector<unsigned char>* v, unsigned int offset) {
    if (v->size() >= 4+ offset)
        return getUint(v->data(), offset);
    return 0;
}

unsigned int getUint(std::vector<unsigned char>* v) {
    return getUint(v, 0);
}
