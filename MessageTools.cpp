#include "MessageTools.h"

void cpyMsgPart(unsigned char* in, unsigned char* out, unsigned int inOffset, unsigned int outOffset, unsigned int length) {
    for (unsigned int i = 0; i < length; i++) {
        out[outOffset + i] = in[inOffset + i];
    }
}

void cpyMsgPart(char* in, unsigned char* out, unsigned int inOffset, unsigned int outOffset, unsigned int length) {
    for (unsigned int i = 0; i < length; i++) {
        out[outOffset + i] = in[inOffset + i];
    }
}

bool compArr(unsigned char* a, unsigned char* b, unsigned int length) {
    return compArr(a, b, 0, 0, length);
}

bool compArr(unsigned char* a, unsigned char* b, unsigned int aOffset, unsigned int bOffset, unsigned int length) {
    for (unsigned int i = 0; i < length; i++) {
        if (a[aOffset + i] != b[bOffset + i])
            return  false;
    }
    return true;
}

void getBytesWithOffset(unsigned char* buffer, int bitOffset, uint64_t bitLength, unsigned char* out) {
    uint64_t lengthBytes = bitLength / 8;
    for (uint64_t i = 0; i < lengthBytes; i++) {
        out[i] = getByteWithOffset(buffer, bitOffset + i * 8);
    }

    int lengthMod = bitLength % 8;
    if (lengthMod != 0) {
        out[lengthBytes - 1] &= (unsigned char)0xff << (8 - lengthMod);
    }
}

unsigned char getByteWithOffset(unsigned char* buffer, int bitOffset) {
    int bitOffsetMod = bitOffset % 8;
    int byteOffset = bitOffset / 8;
    if (bitOffsetMod == 0) {
        return buffer[byteOffset];
    } else {
        return (buffer[byteOffset] << bitOffsetMod) | ((unsigned char)(buffer[byteOffset + 1]) >> bitOffsetMod);
    }
}

uchar_sp makeSharedUCharArrayPtr(size_t size) {
    return uchar_sp(new uchar[size], array_deleter<uchar>());
}

uchar_sp makeSharedUCharArrayPtr(std::string s) {
    uchar_sp ptr = makeSharedUCharArrayPtr(s.length());
    memcpy(ptr.get(), s.c_str(), s.length());
    return ptr;
}

uchar_sp makeSharedUCharArrayPtr(uchar* s, size_t l) {
    uchar_sp ptr = makeSharedUCharArrayPtr(l);
    memcpy(ptr.get(), s, l);
    return ptr;
}

uchar_sp makeSharedUCharArrayPtr_del(uchar* s, size_t l) {
    uchar_sp ptr = makeSharedUCharArrayPtr(s, l);
    delete[] s;
    return ptr;
}

void printByteArray(uchar *c, int length) {
    for (int i = 0; i < length; ++i) {
        printByte(c[i]);
    }
    std::cout << std::endl;
}

void printByte(uchar c) {
    std::cout << std::bitset<8>(c) << ' ';
}

ADDR makeSharedAddrPtr(const char* data) {
    ADDR addr = std::make_shared<std::array<uchar, 6>>();
    memcpy(addr.get()->data(), data, 6);
    return addr;
}
