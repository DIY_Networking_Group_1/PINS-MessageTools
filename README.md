# PINS-MessageTools

# Description:

C/C++ Useful tools for working with Messages. It was used by ([PINS-BM](https://gitlab.com/DIY_Networking_Group_1/PINS-BM), [PINS-Lora-transfer](https://gitlab.com/DIY_Networking_Group_1/PINS-Lora-transfer), [PINS-PTM](https://gitlab.com/DIY_Networking_Group_1/PINS-PTM)). It contains for example the parsers for the different message types.

### [Ptm](https://gitlab.com/DIY_Networking_Group_1/PINS-MessageTools/tree/master/ptm):
Is the parser for [Ptm prtocol](https://gitlab.com/DIY_Networking_Group_1/PINS-Proctocoll/wikis/PTM-Protocol), for communication between BM and PTM-device.

### [Serial](https://gitlab.com/DIY_Networking_Group_1/PINS-MessageTools/tree/master/serial):
Is the parser for the [Serial protocol](https://gitlab.com/DIY_Networking_Group_1/PINS-Proctocoll/wikis/serial-protocol) used for communication between BM and ESP32.

### [Toplevel protocol](https://gitlab.com/DIY_Networking_Group_1/PINS-Proctocoll/wikis/Toplevel-Protocol):
Is the parser for the [Toplevel protocol](https://gitlab.com/DIY_Networking_Group_1/PINS-Proctocoll/wikis/Toplevel-Protocol) used to encode the high level messages between two BM's.

### [Binary Helper](https://gitlab.com/DIY_Networking_Group_1/PINS-MessageTools/blob/master/BinaryHelper.h):

Contains methods used to convert several file types from and to to unsigned char arrays.

### [Message Consts](https://gitlab.com/DIY_Networking_Group_1/PINS-MessageTools/blob/master/MessageConsts.h):

Contains some type macros.

### [Message Enums](https://gitlab.com/DIY_Networking_Group_1/PINS-MessageTools/blob/master/MessageEnums.h):

Contains the definition for the internal state enumeration.

### [Message Queue](https://gitlab.com/DIY_Networking_Group_1/PINS-MessageTools/blob/master/MessageQueue.cpp):

Is the definition for the thread save queue which is used for inter thread communication between the components of PINS-BM. Warning if used you need to also include the cpp (Due to problems with the template type)

### [Message Tools](https://gitlab.com/DIY_Networking_Group_1/PINS-MessageTools/blob/master/MessageQueue.cpp):

Contains useful method when working with char arrays

## Dependencies:

To compile PINS-MessageTools you need a C++17 capable compiler (G++ >= 7 , Clang++ >= 6).

## Installation:

Because this is a library you don't have to install it. You should only add it as a submodule and import the desired code.
