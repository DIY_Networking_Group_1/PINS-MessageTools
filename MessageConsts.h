#pragma once

typedef std::array<unsigned char, 6> ADDRTYPE;
typedef std::shared_ptr<ADDRTYPE> ADDR;
