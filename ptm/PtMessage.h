#pragma once

#include <vector>
#include <memory>

#include "../MessageEnums.h"
#include "../MessageTools.h"
#include "../BinaryHelper.h"
#include "../MessageConsts.h"

enum PtType {
    pt_reserved = 0b0,
    pt_get_base_addr = 0b1,
    pt_send_base_addr = 0b10,
    pt_no_messages = 0b11,
    pt_send_message = 0b100,
    pt_ack_last_message = 0b101,
    pt_ptm_full = 0b110,
};

class PtMessage {
 private:
    internalState intState;
    ADDR address;
    bool isParsed = false;
    unsigned char* payload = NULL;
    unsigned int payloadLength = 0;
    PtType msgType;

    void parseMessage(unsigned char*, unsigned int);
    unsigned char toChar(PtType);
    PtType toPtType(unsigned char);
    bool isValidType(PtType);
    bool hasPayload(PtType);
    bool hasAddr(PtType);

 public:
    explicit PtMessage(PtType);
    explicit PtMessage(std::vector<unsigned char>*);
    PtMessage(PtType, ADDR);
    PtMessage(PtType, unsigned char*, unsigned int);
    PtMessage(PtType, ADDR, unsigned char*, unsigned int);
    PtMessage(unsigned char*, unsigned int);
    ~PtMessage();

    void freePayload();
    void genMsg(unsigned char*);
    void genMsg(std::vector<unsigned char>*);
    unsigned char* genMsg();

    PtType getType();
    internalState getState();
    ADDR getAddr();
    unsigned char* getTAddr();
    unsigned char* getPayload();
    unsigned int getPayloadLength();
    unsigned int getMsgLength();

    bool compare(PtMessage*);
};

