#include "PtMessage.h"

PtMessage::PtMessage(PtType t) {
    this->msgType = t;
    this->intState = ((!hasPayload(t) && !hasAddr(t)) ? i_success: i_error);
}

PtMessage::PtMessage(std::vector<unsigned char>* v) {
    PtMessage(v->data(), v->size());
}

PtMessage::PtMessage(PtType t, ADDR a): address(a) {
    this->msgType = t;
    this->intState = ((!hasPayload(t) && hasAddr(t)) ? i_success: i_error);
}

PtMessage::PtMessage(PtType t, unsigned char* c, unsigned int len) {
    this->msgType = t;
    this->payload = c;
    this->payloadLength = len;
    this->intState = ((hasPayload(t) && !hasAddr(t)) ? i_success: i_error);
}

PtMessage::PtMessage(PtType t, ADDR a, unsigned char* c, unsigned int l): address(a) {
    this->msgType = t;
    this->payload = c,
    this->payloadLength = l;
    this->intState = ((hasPayload(t) && hasAddr(t)) ? i_success: i_error);
}

PtMessage::PtMessage(unsigned char* c, unsigned int len) {
    parseMessage(c, len);
}

PtMessage::~PtMessage() {
    if (this->isParsed) {
        delete[] this->payload;
    }
}


unsigned char PtMessage::toChar(PtType t) {
    switch (t) {
        case pt_get_base_addr:
            return 1;
        case pt_send_base_addr:
            return 2;
        case pt_no_messages:
            return 3;
        case pt_send_message:
            return 4;
        case pt_ack_last_message:
            return 5;
        case pt_ptm_full:
            return 6;
        default:
            return 0;
    }
}

PtType PtMessage::toPtType(unsigned char c) {
    switch (c) {
        case 1:
            return pt_get_base_addr;
        case 2:
            return pt_send_base_addr;
        case 3:
            return pt_no_messages;
        case 4:
            return pt_send_message;
        case 5:
            return pt_ack_last_message;
        case 6:
            return pt_ptm_full;
        default:
            return pt_reserved;
    }
}

bool PtMessage::isValidType(PtType t) {
    switch (t) {
        case pt_get_base_addr:
            return true;
        case pt_send_base_addr:
            return true;
        case pt_no_messages:
            return true;
        case pt_send_message:
            return true;
        case pt_ack_last_message:
            return true;
        case pt_ptm_full:
            return true;
        default:
            return false;
    }
}

bool PtMessage::hasPayload(PtType t) {
    switch (t) {
        case pt_get_base_addr:
            return false;
        case pt_send_base_addr:
            return false;
        case pt_no_messages:
            return false;
        case pt_send_message:
            return true;
        case pt_ack_last_message:
            return false;
        case pt_ptm_full:
            return false;
        default:
            return false;
    }
}

bool PtMessage::hasAddr(PtType t) {
    switch (t) {
        case pt_get_base_addr:
            return false;
        case pt_send_base_addr:
            return true;
        case pt_no_messages:
            return false;
        case pt_send_message:
            return true;
        case pt_ack_last_message:
            return false;
        case pt_ptm_full:
            return false;
        default:
            return false;
    }
}

void PtMessage::freePayload() {
    this->isParsed = false;
}

void PtMessage::parseMessage(unsigned char* c, unsigned int len) {
    if (len >= 1) {
        this->isParsed = true;
        this->msgType = (PtType)c[0];
        if (isValidType(this->msgType) && getMsgLength() <= len) {
            if (hasAddr(this->msgType)) {
                this->address = std::make_shared<ADDRTYPE>();
                cpyMsgPart(c, this->address.get()->data(), 1, 0, 6);
            }
            if (hasPayload(this->msgType)) {
                this->payloadLength = getUint(c, 1 + (hasAddr(this->msgType) ? 6 : 0));
                if (this->getMsgLength() <= len) {
                    this->payload = new unsigned char[this->payloadLength];
                    cpyMsgPart(c, this->payload, 5 + (hasAddr(this->msgType) ? 6 : 0), 0, this->payloadLength);
                } else {
                    this->intState = i_error;
                    return;
                }
            }
            this->intState = i_success;
            return;
        }
    }
    this->intState = i_error;
    return;
}


void PtMessage::genMsg(unsigned char* c) {
    if (this->intState == i_success) {
        c[0] = (unsigned char)this->msgType;
        if (hasAddr(this->msgType)) {
            cpyMsgPart(this->address.get()->data(), c, 0, 1, 6);
        }
        if (hasPayload(this->msgType)) {
            setUint(c, 1 + (hasAddr(this->msgType) ? 6 : 0), this->payloadLength);
            cpyMsgPart(this->payload, c, 0, 5 + (hasAddr(this->msgType) ? 6 : 0), this->payloadLength);
        }
    }
}

void PtMessage::genMsg(std::vector<unsigned char>* v) {
    if (v->size() < getMsgLength()) {
        v->resize(getMsgLength());
    }
    genMsg(v->data());
}

unsigned char* PtMessage::genMsg() {
    unsigned char* ret = new unsigned char[getMsgLength()];
    genMsg(ret);
    return ret;
}


PtType PtMessage::getType() {
    return this->msgType;
}

internalState PtMessage::getState() {
    return this->intState;
}

ADDR PtMessage::getAddr() {
    return this->address;
}

unsigned char* PtMessage::getTAddr() {
    return this->address.get()->data();
}

unsigned char* PtMessage::getPayload() {
    return this->payload;
}

unsigned int PtMessage::getPayloadLength() {
    return this->payloadLength;
}

unsigned int PtMessage::getMsgLength() {
    return 1 + (this->hasAddr(this->msgType) ? 6 : 0) + (this->hasPayload(this->msgType) ? this->payloadLength + 4  : 0);
}


bool PtMessage::compare(PtMessage* other) {
    if (this->intState == i_success && other->getState() == i_success) {
        if (this->msgType == other->getType()) {
            if (hasAddr(this->msgType)) {
                if (!compArr(this->address.get()->data(), other->getTAddr(), 0, 0, 6)) {
                    return false;
                }
            }
            if (hasPayload(this->msgType)) {
                if (this->payloadLength == other->getPayloadLength()) {
                    return compArr(this->payload, other->getPayload(), 0, 0, this->payloadLength);
                }
                return false;
            }
            return true;
        }
    }
    return false;
}
