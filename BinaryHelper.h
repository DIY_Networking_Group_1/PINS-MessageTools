#pragma once

#include <vector>
#include <cstdint>

void setUint64(unsigned char* c, unsigned int offset, uint64_t in);
void setUint64(unsigned char* c, uint64_t in);
void setUint64(std::vector<unsigned char>* v, unsigned int offset, uint64_t in);
void setUint64(std::vector<unsigned char>* v, uint64_t in);
uint64_t getUint64(unsigned char* c, unsigned int offset);
uint64_t getUint64(unsigned char* c);
uint64_t getUint64(std::vector<unsigned char>* v, unsigned int offset);
uint64_t getUint64(std::vector<unsigned char>* v);
void setUint16(unsigned char* c, unsigned int offset, uint16_t in);
void setUint16(unsigned char* c, uint16_t in);
void setUint16(std::vector<unsigned char>* v, unsigned int offset, uint16_t in);
void setUint16(std::vector<unsigned char>* v, uint16_t in);
uint16_t getUint16(unsigned char* c, unsigned int offset);
uint16_t getUint16(unsigned char* c);
uint16_t getUint16(std::vector<unsigned char>* v, unsigned int offset);
uint16_t getUint16(std::vector<unsigned char>* v);
void setUint(unsigned char* c, unsigned int offset, unsigned int in);
void setUint(std::vector<unsigned char>* v, unsigned int in);
void setUint(std::vector<unsigned char>* v, unsigned int offset, unsigned int in);
void setUint(std::vector<unsigned char>* v);
unsigned int getUint(unsigned char* c, int offset);
unsigned int getUint(unsigned char* c);
unsigned int getUint(std::vector<unsigned char>* v, unsigned int offset);
unsigned int getUint(std::vector<unsigned char>* v);
