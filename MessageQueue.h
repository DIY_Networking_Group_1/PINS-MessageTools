#pragma once
#include <mutex>
#include <condition_variable>
#include <vector>

template <typename T>

class MessageQueue {
 private:
    std::mutex queueLock;
    std::condition_variable cv;
    std::vector<T> queueVec;

 public:
    void push(T const);
    T pop();
    inline unsigned int size();
    T get(unsigned int);
    inline bool isEmpty();
};
