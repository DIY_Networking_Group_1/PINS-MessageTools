#pragma once

#include <stdint.h>
#include <string.h>
#include <memory>
#include <bitset>
#include <iostream>
#include <string>
#include "MessageConsts.h"

typedef unsigned char uchar;
typedef std::shared_ptr<uchar> uchar_sp;

// For deleting shared array pointer
template< typename T >
struct array_deleter {
    void operator()(T const * p) {
        delete[] p;
    }
};

/**
 * Creates a prior C++17 shared pointer for an unsigned char array with given size.
 */
uchar_sp makeSharedUCharArrayPtr(size_t size);

/**
 * Creates a prior C++17 shared pointer for an unsigned char array.
 * It get initialised with the given string.
 */
uchar_sp makeSharedUCharArrayPtr(std::string s);

/**
 * Creates a prior C++17 shared pointer for an unsigned char array.
 * It get initialised with the given unsigned char array.
 */
uchar_sp makeSharedUCharArrayPtr(uchar* s, size_t l);

/**
 * Creates a prior C++17 shared pointer for an unsigned char array.
 * It get initialised with the given unsigned char array.
 * Once created deletes the given char array.
 */
uchar_sp makeSharedUCharArrayPtr_del(uchar* s, size_t l);

ADDR makeSharedAddrPtr(const char* data);

/**
 * Prints the given unsigned char to stdout.
 */
void printByte(uchar c);

/**
 * Prints the given unsigned char array to stdout.
 */
void printByteArray(uchar *c, int length);

void cpyMsgPart(unsigned char* in, unsigned char* out, unsigned int inOffset, unsigned int outOffset, unsigned int length);
void cpyMsgPart(char* in, char* out, unsigned int inOffset, unsigned int outOffset, unsigned int length);

void getBytesWithOffset(unsigned char* buffer, int bitOffset, uint64_t bitLength, unsigned char* out);
unsigned char getByteWithOffset(unsigned char* buffer, int bitOffset);

bool compArr(unsigned char* a, unsigned char* b, unsigned int length);
bool compArr(unsigned char* a, unsigned char* b, unsigned int aOffset, unsigned int bOffset, unsigned int length);
