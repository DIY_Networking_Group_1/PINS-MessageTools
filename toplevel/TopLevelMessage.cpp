#include "TopLevelMessage.h"

TopMsg::TopMsg(TopType t, ADDR  dstAddr, ADDR srcAddr, unsigned char seqNr): srcAddr(srcAddr), dstAddr(dstAddr) {
    this->msgType = t;
    this->seqNr = seqNr;
    this->intState = hasPayload(this->msgType) ? i_error : i_success;
}

TopMsg::TopMsg(TopType t, ADDR dstAddr, ADDR srcAddr, unsigned char seqNr, unsigned char* payload, unsigned int payloadLength): srcAddr(srcAddr), dstAddr(dstAddr) {
    this->msgType = t;
    this->seqNr = seqNr;
    this->payload = payload;
    this->payloadLength = payloadLength;
    this->intState = hasPayload(this->msgType) ? i_success : i_error;
}

TopMsg::TopMsg(unsigned char* c, unsigned int length) {
    this->isParsed = true;
    parseMessage(c, length);
}

TopMsg::~TopMsg() {
    if (isParsed) {
        delete[] this->payload;
    }
}

TopType TopMsg::toTopType(unsigned char c) {
    switch (c) {
        case 1:
            return to_ping;
        case 2:
            return to_ack;
        case 3:
            return to_sendbytes;
        default:
            return to_reserved;
    }
}

unsigned char TopMsg::toChar(TopType t) {
    switch (t) {
        case to_ping:
            return 1;
        case to_ack:
            return 2;
        case to_sendbytes:
            return 3;
        default:
            return 0;
    }
}

bool TopMsg::isValidType(TopType t) {
    switch (t) {
        case to_ping:
            return true;
        case to_ack:
            return true;
        case to_sendbytes:
            return true;
        default:
            return false;
    }
}

bool TopMsg::hasPayload(TopType t) {
    switch (t) {
        case to_sendbytes:
            return true;
        default:
            return false;
    }
}

void TopMsg::freePayload() {
    this->isParsed = false;
}

void TopMsg::parseMessage(unsigned char* c, unsigned int len) {
    if (len < 14) {
        this->intState = i_error;
        return;
    }
    this->dstAddr = std::make_shared<ADDRTYPE>();
    this->srcAddr = std::make_shared<ADDRTYPE>();
    cpyMsgPart(c, this->dstAddr.get()->data(), 0, 0, 6);
    cpyMsgPart(c, this->srcAddr.get()->data(), 6, 0, 6);
    this->seqNr = c[12];
    this->msgType = (TopType) c[13];
    if (hasPayload(this->msgType)) {
        this->payloadLength = getUint(c, 14);
        if (len < getMsgLength() && isValidType(this->msgType)) {
            this->intState = i_error;
            return;
        }
        this->payload = new unsigned char[this->payloadLength];
        cpyMsgPart(c, this->payload, 18, 0, this->payloadLength);
    }
    this->intState = i_success;
    this->isParsed = true;
}

void TopMsg::genMsg(unsigned char* c) {
    cpyMsgPart(this->dstAddr.get()->data(), c, 0, 0, 6);
    cpyMsgPart(this->srcAddr.get()->data(), c, 0, 6, 6);
    c[12] = this->seqNr;
    c[13] = (unsigned char) this->msgType;
    if (hasPayload(this->msgType)) {
        setUint(c, 14, this->payloadLength);
        cpyMsgPart(this->payload, c, 0, 18, this->payloadLength);
    }
}

unsigned char* TopMsg::genMsg() {
    unsigned char* c = new unsigned char[getMsgLength()];
    genMsg(c);
    return c;
}

void TopMsg::genMsg(std::vector<unsigned char>* v) {
    if (v->size() < getMsgLength()) {
        v->resize(getMsgLength());
    }
    genMsg(v->data());
}

TopType TopMsg::getType() {
    return this->msgType;
}

internalState TopMsg::getState() {
    return this->intState;
}

unsigned char TopMsg::getSeqNr() {
    return this->seqNr;
}

unsigned char* TopMsg::getPayload() {
    return this->payload;
}

unsigned int TopMsg::getPayloadLength() {
    return this->payloadLength;
}

unsigned int TopMsg::getMsgLength() {
    return 14 + (hasPayload(this->msgType) ? (4 + this->payloadLength) : 0);
}

ADDR TopMsg::getSRC() {
    return this->srcAddr;
}

unsigned char* TopMsg::tmpGetSRC() {
    return this->srcAddr.get()->data();
}

ADDR TopMsg::getDest() {
    return this->dstAddr;
}

unsigned char* TopMsg::tmpGetDest() {
    return this->dstAddr.get()->data();
}

bool TopMsg::compare(TopMsg *other) {
    if (this->intState == i_success && other->getState() == i_success) {
        if (this->msgType == other->getType()) {
            if (compArr(this->srcAddr.get()->data(), other->tmpGetSRC(), 6)) {
                if (!compArr(this->dstAddr.get()->data(), other->tmpGetDest(), 6)) {
                    return false;
                }
                if (hasPayload(this->msgType)) {
                    if (this->payloadLength != other->getPayloadLength()) {
                        return false;
                    } else if (!compArr(this->payload, other->getPayload(), this->payloadLength)) {
                        return false;
                    }
                }
                return true;
            }
        }
    }
    return false;
}

bool TopMsg::strictCompare(TopMsg *other) {
    if (this->seqNr == other->getSeqNr()) {
        return compare(other);
    }
    return false;
}
