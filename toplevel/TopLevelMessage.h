#pragma once

#include <cstdint>
#include <cstring>
#include <vector>
#include <memory>

#include "../MessageEnums.h"
#include "../MessageTools.h"
#include "../BinaryHelper.h"
#include "../MessageConsts.h"



enum TopType {
    to_reserved = 0b0,
    to_ping = 0b1,
    to_ack = 0b10,
    to_sendbytes = 0b11,
};

class TopMsg {
 private:
    internalState intState;
    ADDR srcAddr;
    ADDR dstAddr;
    bool isParsed = false;
    unsigned char* payload = NULL;
    unsigned int payloadLength = 0;
    unsigned char seqNr;
    TopType msgType;

    unsigned char toChar(TopType);
    TopType toTopType(unsigned char);
    void parseMessage(unsigned char* c, unsigned int length);
    bool isValidType(TopType t);
    bool hasPayload(TopType t);

 public:
    TopMsg(TopType t, ADDR  dstAddr, ADDR srcAddr, unsigned char seqNr);
    TopMsg(TopType t, ADDR dstAddr, ADDR srcAddr, unsigned char seqNr, unsigned char* payload, unsigned int payloadLength);
    TopMsg(unsigned char* c, unsigned int length);
    ~TopMsg();

    void freePayload();
    void genMsg(unsigned char* c);
    unsigned char* genMsg();
    void genMsg(std::vector<unsigned char>* v);

    TopType getType();
    internalState getState();
    unsigned char getSeqNr();
    unsigned char* getPayload();
    unsigned int getPayloadLength();
    unsigned int getMsgLength();
    ADDR getSRC();
    ADDR getDest();
    unsigned char* tmpGetSRC();
    unsigned char* tmpGetDest();

    bool compare(TopMsg* other);
    bool strictCompare(TopMsg* other);
};
