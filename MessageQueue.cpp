#ifndef _MESSAGEQUE_CPP_
#define _MESSAGEQUE_CPP_
#include "MessageQueue.h"


template <typename T>
void MessageQueue<T>::push(T const input) {
    std::unique_lock<std::mutex> lck(queueLock);
    this->queueVec.push_back(input);
    this->cv.notify_one();
}

template <typename T>
T MessageQueue<T>::pop() {
    std::unique_lock<std::mutex> lck(queueLock);
    while (isEmpty()) {
        this->cv.wait(lck);
    }
    T item = queueVec.front();
    this->queueVec.erase(queueVec.begin());
    return item;
}

template <typename T>
inline unsigned int MessageQueue<T>::size() {
    return this->queueVec.size();
}

template <typename T>
T MessageQueue<T>::get(unsigned int i) {
    if (this->queueVec.size() > i) {
        std::unique_lock<std::mutex> lck(queueLock);
        T item = this->queueVec[i];
        return item;
    }
    return NULL;
}

template <typename T>
inline bool MessageQueue<T>::isEmpty() {
    return this->size() == 0;
}
#endif
