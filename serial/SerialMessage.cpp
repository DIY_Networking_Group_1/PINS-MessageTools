#include "SerialMessage.h"

SerialMsg::SerialMsg(SerialType t) {
    this->msgType = t;
    this->payloadLength = 0;
    this->intState = ((!hasAddress(this->msgType) && !hasPayload(this->msgType)) ? i_success : i_error);
}

SerialMsg::SerialMsg(SerialType t, ADDR dstAddr, std::vector<unsigned char>* v) {
    SerialMsg(t, dstAddr, v->data(), v->size());
}

SerialMsg::SerialMsg(SerialType t, ADDR dstAddr, unsigned char* c, unsigned int length): dstAddr(dstAddr) {
    this->payloadLength = length;
    this->payload = c;
    this->msgType = t;
    this->intState = ((hasPayload(this->msgType) && hasAddress(this->msgType)) ? i_success : i_error);
}

SerialMsg::SerialMsg(SerialType t, unsigned char* c, unsigned int length) {
    this->msgType = t;
    this->payload = c;
    this->payloadLength = length;
    this->intState = ((hasPayload(this->msgType) && !hasAddress(this->msgType)) ? i_success : i_error);
}

SerialMsg::SerialMsg(SerialType t, ADDR dstAddr): dstAddr(dstAddr) {
    this->msgType = t;
    this->intState = ((!hasPayload(this->msgType) && hasAddress(this->msgType)) ? i_success : i_error);
}

SerialMsg::~SerialMsg() {
    if (isParsed) {
        delete[] this->payload;
    }
}

SerialType SerialMsg::toStype(unsigned char c) {
    switch (c) {
        case 1:
            return t_send_Lora_data;
        case 2:
            return t_update_display;
        case 3:
            return t_send_buffer_full;
        case 4:
            return t_send_buffer_ready;
        case 5:
            return t_received_Lora_data;
        case 6:
            return t_set_Lora_address;
        default:
            return t_reserved;
    }
}

bool SerialMsg::hasPayload(SerialType t) {
    switch (t) {
        case t_send_Lora_data:
            return true;
        case t_update_display:
            return true;
        case t_received_Lora_data:
            return true;
        default:
            return false;
    }
}

bool SerialMsg::hasAddress(SerialType t) {
    switch (t) {
        case t_send_Lora_data:
            return true;
        case t_set_Lora_address:
            return true;
        default:
            return false;
    }
}

unsigned char SerialMsg::toChar(SerialType t) {
    switch (t) {
        case t_send_Lora_data:
            return 1;
        case t_update_display:
            return 2;
        case t_send_buffer_full:
            return 3;
        case t_send_buffer_ready:
            return 4;
        case t_received_Lora_data:
            return 5;
        case t_set_Lora_address:
            return 6;
        default:
            return 0;
    }
}

bool SerialMsg::isValidType(SerialType t) {
    switch (t) {
        case t_send_Lora_data:
            return true;
        case t_update_display:
            return true;
        case t_send_buffer_full:
            return true;
        case t_send_buffer_ready:
            return true;
        case t_received_Lora_data:
            return true;
        case t_set_Lora_address:
            return true;
        default:
            return false;
    }
}

SerialMsg::SerialMsg(std::vector<unsigned char>* v) {
    parseMessage(v->data(), v->size());
}

SerialMsg::SerialMsg(unsigned char* c, unsigned int length) {
    parseMessage(c, length);
}

SerialType SerialMsg::getType() {
    return this->msgType;
}

internalState SerialMsg::getState() {
    return this->intState;
}

void SerialMsg::freePayload() {
    this->isParsed = false;
}


void SerialMsg::parseMessage(unsigned char* c, unsigned int length) {
    if (length < 1) {
        this->intState = i_error;
        return;
    }
    isParsed = true;
    this->msgType = toStype(c[0]);
    if (isValidType(this->msgType) && length >= getMsgLength()) {
        if (hasAddress(this->msgType)) {
            this->dstAddr = std::make_shared<ADDRTYPE>();
            cpyMsgPart(c, this->dstAddr.get()->data(), 1 , 0, 6);
        }
        if (hasPayload(this->msgType)) {
            this->payloadLength = getUint(c, 1 + (hasAddress(this->msgType) ? 6 : 0));
            if (this->getMsgLength() <= length) {
                this->payload = new unsigned char[this->payloadLength];
                cpyMsgPart(c, this->payload, 5 + (hasAddress(this->msgType) ? 6: 0), 0, this->payloadLength);
            } else {
                this->intState = i_error;
                return;
            }
        }
        this->intState = i_success;
        return;
    }
    this->intState = i_error;
    return;
}

unsigned char* SerialMsg::getPayload() {
    return this->payload;
}
unsigned int SerialMsg::getPayloadLength() {
    return this->payloadLength;
}

ADDR SerialMsg::getDST() {
    return this->dstAddr;
}

void SerialMsg::getDST(unsigned char* destAddr) {
    memcpy(destAddr, this->dstAddr.get(), 6);
}

unsigned char* SerialMsg::getTMPDST() {
    return this->dstAddr.get()->data();
}

unsigned int SerialMsg::getMsgLength() {
    return 1 + (this->hasAddress(this->msgType) ? 6 : 0) + (this->hasPayload(this->msgType) ? this->payloadLength + 4  : 0);
}

void SerialMsg::genMsg(unsigned char* c) {
    if (this->intState == i_success) {
        c[0] = toChar(this->msgType);
        if (hasAddress(this->msgType)) {
            cpyMsgPart(this->dstAddr.get()->data(), c, 0, 1, 6);
        }
        if (hasPayload(this->msgType)) {
            setUint(c, 1 + (hasAddress(this->msgType) ? 6 : 0), this->payloadLength);
            cpyMsgPart(this->payload, c, 0, 5 + (hasAddress(this->msgType) ? 6 : 0), this->payloadLength);
        }
    }
}

unsigned char* SerialMsg::genMsg() {
    unsigned char* ret = new unsigned char[getMsgLength()];
    genMsg(ret);
    return ret;
}

void SerialMsg::genMsg(std::vector<unsigned char>* v) {
    if (v->size() < getMsgLength())
        v->resize(getMsgLength());
    genMsg(v->data());
}

bool SerialMsg::compare(SerialMsg* other) {
    if (this->intState == i_success && other->getState() == i_success) {
        if (this->msgType == other->getType()) {
            if (hasAddress(this->msgType)) {
                if (!compArr(this->dstAddr.get()->data(), other->getTMPDST(), 6)) {
                    return false;
                }
            }
            if (hasPayload(this->msgType)) {
                if (this->payloadLength == other->getPayloadLength()) {
                    return compArr(this->payload, other->getPayload(), this->payloadLength);
                }
                return false;
            }
            return true;
        }
    }
    return false;
}
