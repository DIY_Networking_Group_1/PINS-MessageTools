#pragma once

#include <string.h>
#include <cstdint>
#include <cstring>
#include <vector>
#include <memory>

#include "../MessageEnums.h"
#include "../MessageTools.h"
#include "../BinaryHelper.h"
#include "../MessageConsts.h"

enum SerialType {
    t_reserved = 0b0,
    t_send_Lora_data = 0b1,
    t_update_display = 0b10,
    t_send_buffer_full = 0b11,
    t_send_buffer_ready = 0b100,
    t_received_Lora_data = 0b101,
    t_set_Lora_address = 0b110
};

class SerialMsg {
 private:
    internalState intState;
    unsigned char* payload = NULL;
    ADDR dstAddr;
    unsigned int payloadLength = 0;
    bool isParsed = false;
    SerialType msgType;

    SerialType toStype(unsigned char c);
    unsigned char toChar(SerialType t);
    void parseMessage(unsigned char* c, unsigned int length);
    bool isValidType(SerialType t);
    bool hasPayload(SerialType t);
    bool hasAddress(SerialType t);

 public:
    explicit SerialMsg(std::vector<unsigned char>* v);
    explicit SerialMsg(SerialType t);
    SerialMsg(SerialType t, ADDR Dst);
    SerialMsg(SerialType t, unsigned char* c, unsigned int length);
    SerialMsg(SerialType t, ADDR Dst, std::vector<unsigned char>* v);
    SerialMsg(SerialType t, ADDR Dst,  unsigned char* c, unsigned int length);
    SerialMsg(unsigned char* c, unsigned int length);
    ~SerialMsg();

    void freePayload();
    void genMsg(unsigned char* c);
    unsigned char* genMsg();
    void genMsg(std::vector<unsigned char>* v);

    SerialType getType();
    internalState getState();
    unsigned char* getPayload();
    unsigned int getPayloadLength();
    unsigned int getMsgLength();
    ADDR getDST();
    void getDST(unsigned char* destAddr);
    unsigned char* getTMPDST();
    void getPayload(std::vector <unsigned char>* v);

    bool compare(SerialMsg* other);
};
